# polaroid
Backup utility for Mesosphere Marathon apps

## Command-Line Arguments
```
-d, --directory       REQUIRED        Output directory for config files
-i, --input           OPTIONAL        Comma-separated list of desired keys
-m, --marathon        REQUIRED        The full URL to marathon, including "http[s]://"
-p, --port            OPTIONAL        Port to access marathon if nonstandard, default is 8080
```

## Description

Polaroid is a simple utility for extracting Marathon configurations for running applications on Mesos/Marathon.  Currently the only supported containerizer is Docker.  The accepted Marathon keys for `--input` are:

```
[
    'acceptedResourceRoles',
    'args',
    'backoffFactor',
    'backoffSeconds',
    'cmd',
    'constraints',
    'container',
    'cpus',
    'dependencies',
    'deployments',
    'disk',
    'env',
    'executor',
    'fetch',
    'gpus',
    'healthChecks',
    'id',
    'instances',
    'ipAddress',
    'killSelection',
    'labels',
    'maxLaunchDelaySeconds',
    'mem',
    'portDefinitions',
    'ports',
    'readinessChecks',
    'requirePorts',
    'residency',
    'secrets',
    'storeUrls',
    'taskKillGracePeriodSeconds',
    'tasksHealthy',
    'tasksRunning',
    'tasksStaged',
    'tasksUnhealthy',
    'unreachableStrategy',
    'upgradeStrategy',
    'uris',
    'user',
    'version',
    'versionInfo'
]
```

The default list of `--input` keys are the minimum required to submit an app without further interaction (with the intention that
they can then be managed programmatically).  They are:

```
[
    'args',
    'cmd',
    'constraints',
    'container',
    'cpus',
    'disk',
    'env',
    'fetch',
    'gpus',
    'healthChecks',
    'id',
    'instances',
    'labels'
    'maxLaunchDelaySeconds',
    'mem',
    'secrets'
]
```

Inputs specified at command-line are dictionary matched against the accepted output list.  Any that are typos or not on the list will cause the utility to fail and exit.

## Marathon Directory Structure

Polaroid converts Marathon directories into a dot-delimted flat file structure.

At application root:
```
foo > bar > baz > app1
foo > bar > baz > app2
foo > bar > app3
foo > app4
```
becomes:
```
foo.bar.baz.app1.json
foo.bar.baz.app2.json
foo.bar.app3.json
foo.app4.json
```

## TODO List
- Add support for other outputs, e.g. S3
